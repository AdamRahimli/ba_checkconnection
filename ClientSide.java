
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileReader;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.Calendar;
import javax.imageio.ImageIO;
import javax.swing.Timer;

/**
 *
 * @author BirAdam
 */
public class ClientSide {

    public static void main(String[] args) throws Exception {
        while (true) {
            try {
                String ip = null;
                int port = -1;
                BufferedReader br = new BufferedReader(new FileReader("info.txt"));
                String thisLine = null;
                while ((thisLine = br.readLine()) != null) {
                    String[] paths = thisLine.split(":");
                    if (paths[0].equalsIgnoreCase("ip")) {
                        ip = paths[1];
                    } else if (paths[0].equalsIgnoreCase("port")) {
                        port = Integer.valueOf(paths[1]);
                    }
                }
                System.out.println("IP: " + ip);
                System.out.println("PORT: " + port);
                Socket socket = new Socket(ip, port);
                OutputStream outputStream = socket.getOutputStream();

                Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
                Robot bot = new Robot();
                Image img = bot.createScreenCapture(new Rectangle(0, 0, screenSize.width, screenSize.height));

                BufferedImage image = (BufferedImage) img;
                Graphics2D g2d = image.createGraphics();
                Image tmp = image.getScaledInstance(800, 600, Image.SCALE_SMOOTH);
                g2d.setClip(0, 0, 800, 600);
                g2d.drawImage(tmp, 0, 0, null);

                image = image.getSubimage(0, 0, 800, 600);

                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                ImageIO.write(image, "jpg", byteArrayOutputStream);

                byte[] size = ByteBuffer.allocate(4).putInt(byteArrayOutputStream.size()).array();
                outputStream.write(size);
                outputStream.write(byteArrayOutputStream.toByteArray());
                outputStream.flush();
                System.out.println("Flushed: " + System.currentTimeMillis());

                Thread.sleep(12000);
                System.out.println("Closing: " + System.currentTimeMillis());
                socket.close();
                System.out.println("--------------------------------------------");
                Thread.sleep(3600000);
            } catch (Exception ex) {
                Thread.sleep(300000);
            }
        }
    }
}


import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author BirAdam
 */
public class ServerSide implements Runnable {

    private int port = -1;

    public ServerSide() {
    }

    public ServerSide(int port) {
        this.port = port;
    }

    public static void main(String[] args) {
        try {
            BufferedReader br = new BufferedReader(new FileReader("ports.txt"));
            String port = null;
            while ((port = br.readLine()) != null) {
                new Thread(new ServerSide(Integer.valueOf(port))).start();
            }
        } catch (Exception ex) {

        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                ServerSocket serverSocket = new ServerSocket(port);
                Socket socket = serverSocket.accept();
                System.out.println("connect " + port);
                Calendar cal = Calendar.getInstance();
                String photoName = String.valueOf(cal.get(Calendar.DATE)) + String.valueOf(cal.get(Calendar.MONTH)) + String.valueOf(cal.get(Calendar.YEAR)) + "_" + String.valueOf(cal.get(Calendar.HOUR)) + "_" + String.valueOf(cal.get(Calendar.MINUTE));
                InputStream inputStream = socket.getInputStream();
                System.out.println("Reading: " + System.currentTimeMillis());

                byte[] sizeAr = new byte[4];
                inputStream.read(sizeAr);
                int size = ByteBuffer.wrap(sizeAr).asIntBuffer().get();

                byte[] imageAr = new byte[size];
                inputStream.read(imageAr);

                BufferedImage image = ImageIO.read(new ByteArrayInputStream(imageAr));

                System.out.println("Received " + image.getHeight() + "x" + image.getWidth() + ": " + System.currentTimeMillis());
                ImageIO.write(image, "jpg", new File("port_" + port + "_date_" + photoName + ".jpg"));

                serverSocket.close();
                System.out.println("---------------------------------------------------------------------------");
            } catch (IOException ex) {
                Logger.getLogger(ServerSide.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
